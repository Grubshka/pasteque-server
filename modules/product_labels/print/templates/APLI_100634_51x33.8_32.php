<?php
//    Pastèque Web back office, Product labels module
//
//    Copyright (C) 2017 Philippe Corbes
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

const PAPER_SIZE = "A4";
const PAPER_ORIENTATION = "P";

const V_MARGIN = 13.5;
const H_MARGIN = 4;
const COL_SIZE = 51;
const ROW_SIZE = 33.8;
const COL_NUM = 4;
const ROW_NUM = 8;
const H_PADDING = 0;
const V_PADDING = 0;

const LABEL_X = 0;
const LABEL_Y = 0;
const LABEL_WIDTH = 50;
const LABEL_HEIGHT = 13;
const LABEL_DOTS = 9;

const BARCODE_X = 0;
const BARCODE_Y = 13;
const BARCODE_WIDTH = 25;
const BARCODE_HEIGHT = 14;

const BARCODE_TEXT_X = 0;
const BARCODE_TEXT_Y = 27;
const BARCODE_TEXT_HEIGHT = 6;
const BARCODE_TEXT_DOTS = 8;

const PRICE_X = 25;
const PRICE_Y = 13;
const PRICE_WIDTH = 25;
const PRICE_HEIGHT = 10;
const PRICE_DOTS = 15;

const UNIT_X = 25;
const UNIT_Y = 23;
const UNIT_WIDTH = 25;
const UNIT_HEIGHT = 4;
const UNIT_DOTS = 8;

const REF_X = 25;
const REF_Y = 27;
const REF_WIDTH = 25;
const REF_HEIGHT = 6;
const REF_DOTS = 8;
?>
