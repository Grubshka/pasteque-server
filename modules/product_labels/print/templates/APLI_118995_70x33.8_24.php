<?php
//    Pastèque Web back office, Product labels module
//
//    Copyright (C) 2017 Philippe Corbes
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

const PAPER_SIZE = "A4";
const PAPER_ORIENTATION = "P";

const V_MARGIN = 13.3;
const H_MARGIN = 0;
const COL_SIZE = 70;
const ROW_SIZE = 33.8;
const COL_NUM = 3;
const ROW_NUM = 8;
const H_PADDING = 0;
const V_PADDING = 0;

const LABEL_X = 0;
const LABEL_Y = 0;
const LABEL_WIDTH = 68;
const LABEL_HEIGHT = 12;
const LABEL_DOTS = 10;

const BARCODE_X = 0;
const BARCODE_Y = 12;
const BARCODE_WIDTH = 34;
const BARCODE_HEIGHT = 15;

const BARCODE_TEXT_X = 0;
const BARCODE_TEXT_Y = 27;
const BARCODE_TEXT_HEIGHT = 5;
const BARCODE_TEXT_DOTS = 7;

const PRICE_X = 34;
const PRICE_Y = 12;
const PRICE_WIDTH = 34;
const PRICE_HEIGHT = 10;
const PRICE_DOTS = 20;

const UNIT_X = 34;
const UNIT_Y = 22;
const UNIT_WIDTH = 34;
const UNIT_HEIGHT = 5;
const UNIT_DOTS = 7;

const REF_X = 34;
const REF_Y = 27;
const REF_WIDTH = 34;
const REF_HEIGHT = 5;
const REF_DOTS = 7;
?>
