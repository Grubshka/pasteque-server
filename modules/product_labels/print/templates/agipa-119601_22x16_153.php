<?php
//    Pastèque Web back office, Product labels module
//
//    Copyright (C) 2017 Philippe Corbes
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

const PAPER_SIZE = "A4";
const PAPER_ORIENTATION = "P";

const V_MARGIN = 13;
const H_MARGIN = 6;
const COL_SIZE = 22;
const ROW_SIZE = 16;
const COL_NUM = 9;
const ROW_NUM = 17;
const H_PADDING = 0;
const V_PADDING = 0;

const LABEL_X = -0.1;
const LABEL_Y = 0;
const LABEL_WIDTH = 21;
const LABEL_HEIGHT = 6;
const LABEL_DOTS = 4;

const BARCODE_X = -0.1;
const BARCODE_Y = 6;
const BARCODE_WIDTH = 21;
const BARCODE_HEIGHT = 7;

const BARCODE_TEXT_X = 0;
const BARCODE_TEXT_Y = 13;
const BARCODE_TEXT_HEIGHT = 3;
const BARCODE_TEXT_DOTS = 7;

const PRICE_X = 0.5;
const PRICE_Y = 0.5;
const PRICE_WIDTH = 21;
const PRICE_HEIGHT = 10;
const PRICE_DOTS = 14;

const UNIT_X = 0.5;
const UNIT_Y = 9;
const UNIT_WIDTH = 21;
const UNIT_HEIGHT = 4;
const UNIT_DOTS = 7;

const REF_X = -0.1;
const REF_Y = 14;
const REF_WIDTH = 10;
const REF_HEIGHT = 2;
const REF_DOTS = 7;
?>
