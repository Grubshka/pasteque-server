<?php
//    Pastèque Web back office, Product labels module
//
//    Copyright (C) 2017 Philippe Corbes
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.


//TODO Solve Delete action when we select a category with the select button. In this case the Delete button do not delete line of the static array. Build this array with JQuery

namespace ProductLabels;

$message = NULL;
$error = NULL;

$categories = \Pasteque\CategoriesService::getAll();
$allProducts = \Pasteque\ProductsService::getAll();
$products = array();
foreach ($allProducts as $product) {
	if ($product->barcode !== NULL && $product->barcode != "") {
		$products[] = $product;
	}
}

//Title
echo \Pasteque\row(\Pasteque\mainTitle(\i18n("Tags", PLUGIN_NAME)));

//Information
\Pasteque\tpl_msg_box($message, $error);

// Select all products of a category
echo "<div id='select'>";
\Pasteque\tpl_form('select', 'category', \Pasteque\CategoriesService::getAll());
echo "</div>";

$dir = opendir("modules/product_labels/print/templates/");
while($f = readdir($dir)) {
	if($f != "." && $f != ".." && $f != "index.php") {
		$values[] = substr($f,0,strpos($f,".php"));
	}
}
$content = \Pasteque\row(\Pasteque\form_select("format", \i18n("Format", PLUGIN_NAME), $values, $values));
$content .= \Pasteque\row(\Pasteque\form_number("start_from", "1", \i18n("Start from", PLUGIN_NAME), "1", "1"));
$content .= \Pasteque\row(\Pasteque\form_number("h_margin", "0.0", \i18n("Horizontal margin", PLUGIN_NAME), "0.1"));
$content .= \Pasteque\row(\Pasteque\form_number("v_margin", "0.0", \i18n("Vertical margin", PLUGIN_NAME), "0.1"));
$content .= \Pasteque\row(\Pasteque\vanillaDiv("","catalog-picker"));

$table[0][0] = "";
$table[0][1] = \i18n("Product.reference");
$table[0][2] = \i18n("Product.label");
$table[0][3] = \i18n("Product.barcode");
$table[0][4] = "";

if(isset($_GET["category"])) {
	$id = 1;
	$categoryId = $_GET["category"];
	foreach ($products as $product) {
		if ($product->categoryId == $categoryId) {
			$table[$id][0] = "";
            $table[$id][1] = $product->reference;
            $table[$id][2] = $product->label;
            $table[$id][3] = $product->barcode;
            $table[$id][4] = '<input id="line-' . $product->id . '-qty" type="hidden" name="qty-' . $product->id . '" value="1" />';
            $table[$id][4] .= \Pasteque\buttonGroup(\Pasteque\jsDeleteButton(\i18n("Delete"),"javascript:deleteLine('" . $product->id . "');return false;"));
            $id++;
		}
	}
}
$content .= \Pasteque\row(\Pasteque\standardTable($table));

$content .= \Pasteque\row(\Pasteque\form_send());

echo \Pasteque\row(\Pasteque\form_generate("?" . \Pasteque\PT::URL_ACTION_PARAM . "=print&w=pdf&m=" . PLUGIN_NAME . "&n=tags","post",$content));
\Pasteque\init_catalog("catalog", "catalog-picker", "addProduct", $categories, $products);
?>
<script type="text/javascript">
	addProduct = function(productId) {
		var product = catalog.products[productId];
		if (jQuery("#line-" + productId).length == 0) {
			// Add line
			var src;
			if (product['hasImage']) {
				src = "?p=img&w=product&id=" + product['id'];
			} else {
				src = "?p=img&w=product";
			}
			var html = "<tr id=\"line-" + product['id'] + "\">\n";
			html += "<td><img class=\"thumbnail\" src=\"" + src + "\" /></td>\n";
			html += "<td>" + product['reference'] + "</td>\n";
			html += "<td>" + product['label'] + "</td>\n";
			html += "<td>" + product['barcode'] + "</td>\n";
			html += "<td class=\"qty-cell\"><input id=\"line-" + product['id'] + "-qty\" type=\"hidden\" name=\"qty-" + product['id'] + "\" value=\"1\" />";
			html += "<?php echo sprintf(\Pasteque\esc_js(\Pasteque\buttonGroup(\Pasteque\jsDeleteButton(\i18n("Delete"),"%s"))),"javascript:deleteLine('\" + product['id'] + \"');return false;"); ?></td>\n";
			html += "</tr>\n";
			jQuery("tbody").append(html);
		}
	}
	deleteLine = function(productId) {
		jQuery("#line-" + productId).detach();
	}
</script>
