<?php
//    Pastèque Web back office, Users module
//
//    Copyright (C) 2013 Scil (http://scil.coop)
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace BaseCustomers;

$sql = "SELECT * FROM CUSTOMERS";

$fields = array("NAME","FIRSTNAME","LASTNAME","PREPAID","CURDEBT",
        "CURDATE","NOTES","VISIBLE","EXPIREDATE","TARIFAREA_ID",
        "DISCOUNTPROFILE_ID","TAXID","EMAIL","PHONE","PHONE2","FAX",
        "ADDRESS","ADDRESS2","POSTAL","CITY","REGION","COUNTRY",
        "SEARCHKEY","CARD","ID");
$headers = array(\i18n("Customer.dispName"),
        \i18n("Customer.firstName"),\i18n("Customer.lastName"),
        \i18n("Customer.prepaid"),\i18n("Customer.currDebt"),
        \i18n("Customer.debtDate"),\i18n("Customer.note"),
        \i18n("Customer.visible"),\i18n("Customer.expireDate"),
        \i18n("Customer.tariffArea"), \i18n("Customer.discountProfile"),
        \i18n("Customer.number"), \i18n("Customer.email"),
        \i18n("Customer.phone1"),\i18n("Customer.phone2"),
        \i18n("Customer.fax"),\i18n("Customer.addr1"),
        \i18n("Customer.addr2"),\i18n("Customer.zipCode"),
        \i18n("Customer.city"), \i18n("Customer.region"),
        \i18n("Customer.country"), \i18n("Customer.key"),
        \i18n("Customer.card"), \i18n("id"));

$report = new \Pasteque\Report(PLUGIN_NAME, "customers_list",
        \i18n("Customers list", PLUGIN_NAME),
        $sql, $headers, $fields);

$report->addFilter("CURDATE", "\i18nDatetime");
$report->addFilter("EXPIREDATE", "\i18nDatetime");
$report->addFilter("CURDEBT", "\i18nCurr");
$report->addFilter("PREPAID", "\i18nCurr");

\Pasteque\register_report($report);
