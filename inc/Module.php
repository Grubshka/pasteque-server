<?php
//    Pastèque Web back office
//
//    Copyright (C) 2013 Scil (http://scil.coop)
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace Pasteque;

/** Static class to retrieve the list of available/activated modules. */
class ModuleList {

    /** Merge mandatory modules into a module list. */
    private static function mergeMandatory($list) {
        $config = \getConfig();
        $fullList = array_merge($config['mandatory_modules'], $list);
        return array_unique($fullList);
    }

    /** Remove mandatory modules from a module list. */
    private static function unmergeMandatory($list) {
        $config = \getConfig();
        return array_diff($list, $config['mandatory_modules']);
    }

    /** List all modules. Return an array of dir names. */
    public static function listAll() {
        $dir = PT::$ABSPATH . "/modules";
        $list = scandir($dir);
        $dot = array_search(".", $list);
        if ($dot !== false) {
            array_splice($list, $dot, 1);
        }
        $dotdot = array_search("..", $list);
        if ($dotdot !== false) {
            array_splice($list, $dotdot, 1);
        }
        return $list; // TODO: this assumes all files are module directories
    }

    /** List all basic modules. Return an array of dir names. */
    public static function listBase() {
        $dir = PT::$ABSPATH . "/modules";
        $list = scandir($dir);
        $base = array();
        foreach ($list as $module) {
            if (substr($module, 0, 4) == "base") {
                $base[] = $module;
            }
        }
        return ModuleList::mergeMandatory($base);
    }

    public static function listActivated() {
        $pdo = PDOBuilder::getPDO();
        $stmt = $pdo->prepare("SELECT modules FROM APPLICATIONS;");
        if (!$stmt->execute()) {
            $info = $stmt->errorInfo();
            Log::error(sprintf('Unable to get modules from database %s %s',
                            $info[0], $info[2]));
            return array();
        }
        if ($row = $stmt->fetch()) {
            $modules = $row['MODULES'];
            $data = explode(",", $modules);
        }
        // Check for 'all' and 'base' keywords
        if (count($data) > 0) {
            switch ($data[0]) {
            case 'all': return ModuleList::listAll();
            case 'base': return ModuleList::listBase();
            }
        }
        return ModuleList::mergeMandatory($data);
    }

    public static function setActivatedModules($modules) {
        // Keep only valid modules
        $allModules = ModuleList::listAll();
        $validModules = array_intersect($modules, $allModules);
        // Remove mandatory modules (they are added anyway on get)
        $saveModules = ModuleList::unmergeMandatory($validModules);
        $pdo = PDOBuilder::getPDO();
        $stmt = $pdo->prepare('UPDATE APPLICATIONS set modules = :m;');
        $stmt->bindParam(':m', implode(',', $saveModules));
        if (!$stmt->execute()) {
            $info = $stmt->errorInfo();
            Log::error(sprintf('Unable to update module list %s %s',
                            $info[0], $info[2]));
            return false;
        }
        return true;
    }
}

/** @deprecated
 * Legacy function for ModuleList::listActivated. */
function get_loaded_modules($userId) {
    Log::info('Using deprecated function get_loaded_modules');
    return ModuleList::listActivated();
}

/** Definition of a module */
class Module {

    private $name;

    public function __construct($name) {
        $this->name = $name;
    }

    public function getName() {
        return $this->name;
    }
}
