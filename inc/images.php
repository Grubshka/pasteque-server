<?php
//    Pastèque Web back office
//
//    Copyright (C) 2013 Scil (http://scil.coop)
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace Pasteque;

define("PT_THUMBNAIL_DEFAULT_WIDTH", 128);
define("PT_THUMBNAIL_DEFAULT_HEIGHT", 128);
define("PT_THUMBNAIL_DEFAULT_QUALITY", 50);

/** Convert an image to a jpg thumbnail. If target width and height
 * are not specified they are read from config file.
 * @param $data Image data to resize
 * @param $width Target width, if not specified this is read from config
 * @param $height Target height, if not specified this is read form config
 * @param $outputQuality JPG output quality, if not specified this is read
 * from config file.
 * @return True if thumb is written to $outputFileName, binary data if null,
 * false in case of failure.
 */
function img_thumbnail($data, $width = null, $height = null,
        $outputQuality = null) {
    global $config;
    if ($width === null) {
        if (isset($config['thumb_width']) && isset($config['thumb_height'])) {
            $width = $config['thumb_width'];
            $height = $config['thumb_height'];
        } else {
            $width = PT_THUMBNAIL_DEFAULT_WIDTH;
            $height = PT_THUMBNAIL_DEFAULT_HEIGHT;
        }
    }
    if ($outputQuality === null) {
        if (isset($config['thumb_quality'])) {
            $outputQuality = $config['thumb_quality'];
        } else {
            $outputQuality = PT_THUMBNAIL_DEFAULT_QUALITY;
        }
    }
    $imgData = getimagesizefromstring($data);
    $imgWidth = $imgData[0];
    $imgHeight = $imgData[1];
    $imgType = $imgData[2]; // A gd constant
    $widthRatio = $width / $imgWidth;
    $heightRatio = $height / $imgHeight;
    // Use the smallest ratio to resize without cropping
    $ratio = min($widthRatio, $heightRatio);
    $destWidth = round($imgWidth * $ratio);
    $destHeight = round($imgHeight * $ratio);
    // Read input
    $src = imagecreatefromstring($data);
    if ($src === false) {
        return false;
    }
    // Create thumbnail
    $dst = imagecreatetruecolor($destWidth, $destHeight);
    // Copy image
    imagecopyresampled($dst, $src, 0, 0, 0, 0, $destWidth, $destHeight,
            $imgWidth, $imgHeight);
    // Handle jpg rotation
    if ($imgType == IMG_JPG || $imgType == IMAGETYPE_JPEG) {
        $exif = exif_read_data('data://image/jpeg;base64,'
                . \base64_encode($data));
        if ($exif !== false && isset($exif['Orientation'])) {
            $orientation = $exif['Orientation'];
            switch($orientation) {
            case 3:
                $dst = imagerotate($dst, 180, 0);
                break;
            case 6:
                $dst = imagerotate($dst, -90, 0);
                break;
            case 8:
                $dst = imagerotate($dst, 90, 0);
                break;
            }
        }
    }
    // Write output
    ob_start();
    $done = imagejpeg($dst, null, $outputQuality);
    $data = ob_get_contents();
    ob_end_clean();
    if ($done === false) { return false; }
    return $data;
}

/** Prepare an image for database input.
 * @param $data Image data. Null for erase, PT_IMG_KEEP
 * for no update.
 * @param $width The witdh passed to img_thumbnail.
 * @param $height The height passed to img_thumbnail.
 * @param $outputQuality The jpeg quality passed to img_thumbnail.
 * @return The thumbnailed image if an actual image,
 * PT::IMG_KEEP in case of error, null if $data is null. */
function img_prepareForDb($data, $width = null, $height = null,
        $outputQuality = null) {
    if ($data === null or $data == PT::IMG_KEEP) { return $data; }
    $output = img_thumbnail($data, $width, $height, $outputQuality);
    if ($output === false) { return PT::IMG_KEEP; }
    return $output;
}