<?php
//    Pastèque Web back office
//
//    Copyright (C) 2017 Philippe Pary <philippe@pasteque.org>
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.
namespace Pasteque;

/** Server configurator. It manages creation of a config file and
 * initializing database connectivity. */
class Installer {
    static function sanitize_n_default($arguments) {
       foreach ($arguments as $argument) {
         $argument = trim($argument);
       }
       $arguments["dbtype"] = ($arguments["dbtype"]=="")?"mysql":$arguments["dbtype"]; // this is a troll
       $arguments["dbhost"] = ($arguments["dbhost"]=="")?"localhost":$arguments["dbhost"];
       $arguments["dbname"] = ($arguments["dbname"]=="")?"pasteque":$arguments["dbname"];
       $arguments["username"] = ($arguments["username"]=="")?"admin":$arguments["username"];
       $arguments["userpassword"] = ($arguments["userpassword"]=="")?substr(md5(time()),strlen(time())-10,10):$arguments["userpassword"];
       return $arguments;
    }

    static function perform_checks() {
      global $arguments;
      if (!is_writable(PT::$ABSPATH)
        || !is_writable(PT::$ABSPATH."/core_modules/ident/inifile")
        || !is_writable(PT::$ABSPATH."/core_modules/database/inifile")) {
        return "Pasteque's folder must be writable";
      }
      $dsn_mysql = sprintf("mysql:host=%s",$arguments["dbhost"]);
      $dsn_pgsql = sprintf("pgsql:host=%s",$arguments["dbhost"]);
      try {
        $arguments["dbtype"] = "mysql";
        $pdo = new \PDO($dsn_mysql,$arguments["dbuser"],$arguments["dbpassword"]);
      }
      catch (\PDOException $e) {
        try {
          $arguments["dbtype"] = "pgsql";
          $pdo = new \PDO($dsn_pgsql,$arguments["dbuser"],$arguments["dbpassword"]);
        }
        catch (\PDOException $e) {
          $arguments["dbtype"] = "none";
          return "Error connecting to database, check your creditentials";
        }
      }
      return 1;
    }

    static function database_setup() {
      global $arguments;
      // DB log will get displayed if we meet an problem
      $dblog = array();

      // Let's connect first
      $dsn = sprintf("%s:host=%s",$arguments["dbtype"],$arguments["dbhost"]);
      $pdo = new \PDO($dsn,$arguments["dbuser"],$arguments["dbpassword"]);

      // Then, check if database exists
      $databases = $pdo->query("SHOW DATABASES");
      $dbexists = false;
      $dblog[] = sprintf("Looking for database <em>%s</em>…",$arguments["dbname"]);
      foreach($databases as $database) {
        if ($database["Database"] === $arguments["dbname"]) {
          $dbexists = true;
          $dblog[] = "Database found";
        }
      }

      // If database don't exists, try to create it
      if (!$dbexists) {
        $pdo->beginTransaction();

        /* We assume given user is root … 
        * THAT IS SHIT ! WE MUST AVOID IT 
        * so we'll try create a new user
        * for the new database …
        * if we fail, we screw it */
        $arguments["dbuser"] = "pasteque";
        $arguments["dbpassword"] = substr(time(),strlen(time())-10,10);

        $dblog[] = "Try to create new database";
        $sth = $pdo->prepare(sprintf("CREATE DATABASE %s",$arguments["dbname"]));
        if ($sth->execute() === false) {
          $pdo->rollback();
          $error_info = $sth->errorInfo();
          $dblog[] = sprintf("(%s) %s",$error_info[0],$error_info[2]);
          echo sprintf("<pre>%s</pre>",json_encode($dblog));
          return sprintf("Cannot create database <em>%s</em>",$arguments["dbname"]);
        }

        $dblog[] = "Try to create new user";
        $sth = $pdo->prepare("CREATE USER :dbuser@:dbhost IDENTIFIED BY :dbpassword");
        $sth->bindParam(":dbuser",$arguments["dbuser"]);
        $sth->bindParam(":dbhost",$arguments["dbhost"]);
        $sth->bindParam(":dbpassword",$arguments["dbpassword"]);
        if ($sth->execute() === false) {
          $error_info = $sth->errorInfo();
          $dblog[] = sprintf("(%s) %s",$error_info[0],$error_info[2]);
          $pdo->rollback();
          echo sprintf("<pre>%s</pre>",json_encode($dblog));
          return sprintf("Cannot create user <em>%s</em>",$arguments["dbnewuser"]);
        }

        $dblog[] = "Try to grant rights";
        $sth = $pdo->prepare(sprintf("GRANT ALL ON %s.* TO :dbuser@:dbhost",$arguments["dbname"]));
        $sth->bindParam(":dbuser",$arguments["dbuser"]);
        $sth->bindParam(":dbhost",$arguments["dbhost"]);
        if ($sth->execute() === false) {
          $error_info = $sth->errorInfo();
          $dblog[] = sprintf("(%s) %s",$error_info[0],$error_info[2]);
          $pdo->rollback();
          echo sprintf("<pre>%s</pre>",implode("\n",$dblog));
          return sprintf("Cannot grant rights on database <em>%s</em> to user <em>%s</em>",
            $arguments["dbname"],
            $arguments["dbuser"]);
        }
      }
      else {
        // There is nothing to do if database is OK !!
      }

      // At this very point, everything is alright.
      return 1;
    }

    /* For user we need 2 files to write : ident and database */
    static function userconfig_write() {
      global $arguments;
      // First, check if auth dir exists
      if (!file_exists("auth")) {
        mkdir("auth");
      }
      else if (is_file("auth")){
        return "A file called <em>auth</em> exists. Please, erase it first";
      }
      else if (!is_writable("auth")) {
        return "<em>auth</em> folder must be writable";
      }
      // Then, write ident file
      $fhandle=fopen(sprintf(PT::$ABSPATH . '/auth/%s_id.ini',$arguments["username"]),"w");
      fwrite($fhandle,sprintf("password=%s",password_hash($arguments["userpassword"],PASSWORD_DEFAULT)));
      fclose($fhandle);
      // Then, write database file
      $fhandle=fopen(sprintf(PT::$ABSPATH . '/auth/%s_db.ini',$arguments["username"]),"w");
      fprintf($fhandle,"type = %s\n",$arguments["dbtype"]);
      fprintf($fhandle,"host = %s\n",$arguments["dbhost"]);
      fprintf($fhandle,"port = %s\n",($arguments["dbtype"]=="mysql")?"3306":"5432");
      fprintf($fhandle,"name = \"%s\"\n",$arguments["dbname"]);
      fprintf($fhandle,"user = \"%s\"\n",$arguments["dbuser"]);
      fprintf($fhandle,"password = \"%s\"\n",$arguments["dbpassword"]);
      fclose($fhandle);
      return 1;
    }

    /* For software, we have 3 files to write :
     *  1. main config.php
     *  2. ident config file
     *  3. database config file
     * We will assume directories exist, if not, we're in hell
     */
    static function configfile_write() {
      global $arguments;
      // Main config file
      $fhandle=fopen(PT::$ABSPATH . "/config.php","w");
      fprintf($fhandle,"<?php\n");
      fprintf($fhandle,"\$config['core_ident'] = \"inifile\";\n");
      fprintf($fhandle,"\$config['core_database'] = \"inifile\";\n");
      fprintf($fhandle,"\$config['core_modules'] = \"database\";\n");
      fprintf($fhandle,"\$config['template'] = \"pasteque-bootstrap\";\n");
      fprintf($fhandle,"\$config['thumb_width'] = 128;\n");
      fprintf($fhandle,"\$config['thumb_height'] = 128;\n");
      fprintf($fhandle,"\$config['jwt_timeout'] = 600;\n");
      fprintf($fhandle,"\$config['jwt_secret'] = '%s';\n",substr(time(),strlen(time())-10,10));
      fprintf($fhandle,"\$config['mandatory_modules'] = [\n");
      fprintf($fhandle,"    \"base_backup\",\n");
      fprintf($fhandle,"    \"base_cashes\",\n");
      fprintf($fhandle,"    \"base_cashregisters\",\n");
      fprintf($fhandle,"    \"base_products\",\n");
      fprintf($fhandle,"    \"base_sales\",\n");
      fprintf($fhandle,"    \"modules_management\",\n");
      fprintf($fhandle,"];\n");
      fprintf($fhandle,"\$config['free_modules'] = [\n");
      fprintf($fhandle,"    \"advanced_reports\",\n");
      fprintf($fhandle,"    \"base_accounts\",\n");
      fprintf($fhandle,"    \"base_currencies\",\n");
      fprintf($fhandle,"    \"base_customers\",\n");
      fprintf($fhandle,"    \"base_paymentmodes\",\n");
      fprintf($fhandle,"    \"base_resources\",\n");
      fprintf($fhandle,"    \"base_restaurant\",\n");
      fprintf($fhandle,"    \"base_stocks\",\n");
      fprintf($fhandle,"    \"base_users\",\n");
      fprintf($fhandle,"    \"customer_discountprofiles\",\n");
      fprintf($fhandle,"    \"product_attributes\",\n");
      fprintf($fhandle,"    \"product_barcodes\",\n");
      fprintf($fhandle,"    \"product_compositions\",\n");
      fprintf($fhandle,"    \"product_discounts\",\n");
      fprintf($fhandle,"    \"product_providers\",\n");
      fprintf($fhandle,"    \"product_tariffareas\",\n");
      fprintf($fhandle,"    \"stock_multilocations\",\n");
      fprintf($fhandle,"    \"ticket_discounts\",\n");
      fprintf($fhandle,"];\n");
      fprintf($fhandle,"\$config[\"debug\"] = true;\n");
      fprintf($fhandle,"\$config[\"no_auth\"] = false;\n");
      fprintf($fhandle,"\$config[\"log_level\"] = \"%s\";\n",$arguments["debug"]);
      fprintf($fhandle,"function getConfig() {\n");
      fprintf($fhandle,"    global \$config;\n");
      fprintf($fhandle,"    return \$config;\n");
      fprintf($fhandle,"}\n");
      fclose($fhandle);
      // Ident file
      $fhandle=fopen(PT::$ABSPATH . "/core_modules/ident/inifile/config.php","w");
      fprintf($fhandle,"<?php\n\$config[\"path\"] = \"%s/auth\";",PT::$ABSPATH);
      fclose($fhandle);
      // Database file
      $fhandle=fopen(PT::$ABSPATH . "/core_modules/database/inifile/config.php","w");
      fprintf($fhandle,"<?php\n\$config[\"path\"] = \"%s/auth\";",PT::$ABSPATH);
      fclose($fhandle);
      return 1;
    }
}
