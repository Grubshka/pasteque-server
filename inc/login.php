<?php
//    Pastèque Web back office
//
//    Copyright (C) 2013 Scil (http://scil.coop)
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace Pasteque;

/** Static class to check and generate JWT. */
class Login {

    /** The header, cookie, get or post param to put the auth token in. */
    const TOKEN_PARAM = 'token';
    /** Cache of user id to parse a token only once and fresh connection.*/
    private static $cachedUser;

    private function __construct() {}

    /** Check for an available token. In order it checks in:
     * header, post, get, cookie. */
    public static function getToken() {
        if (!empty($_SERVER['HTTP_' . strtoupper(Login::TOKEN_PARAM)])) {
            Log::debug('Got a token from header param');
            return $_SERVER['HTTP_' . strtoupper(Login::TOKEN_PARAM)];
        } elseif (!empty($_POST[Login::TOKEN_PARAM])) {
            Log::debug('Found a token in POST');
            return $_POST[Login::TOKEN_PARAM];
        } else if (!empty($_GET[Login::TOKEN_PARAM])) {
            Log::debug('Found a token in GET');
            return $_GET[Login::TOKEN_PARAM];
        } else if (!empty($_COOKIE[Login::TOKEN_PARAM])) {
            Log::debug('Got a token from cookie');
            return $_COOKIE[Login::TOKEN_PARAM];
        } else {
            Log::debug('No token found');
            return null;
        }
    }

    /** Check if a token is valid and return user id if true.
     * @return User id if the token is valid, null otherwise. */
    public static function getLoggedUser($token = null) {
        // Check for "no-login" debug feature
        global $config;
        if ($config['debug'] && isset($config['no_auth']) && $config['no_auth']) {
            // Disabled login
            return 0;
        }
        // Look in cache
        if (Login::$cachedUser !== null) {
            return Login::$cachedUser;
        }
        // Get auth token
        if ($token === null) {
            $token = Login::getToken();
        }
        if ($token === null) {
            return null; // No token
        }
        $jwt = JWT::decode($token, $config['jwt_secret']);
        if ($jwt === null) {
            return null; // Error: malformed token
        }
        if ($jwt->isValid($config['jwt_timeout']) ){
            Login::$cachedUser = $jwt->payload['user'];
            return $jwt->payload['user'];
        }
        return null;
    }

    /** Generate a JWT string for a given user. */
    public static function issueToken($user) {
        global $config;
        if (array_search('sha512', hash_algos())) {
            $header = array('alg' => 'HS512');
        } else if (array_search('sha256', hash_algos())) {
            $header = array('alg' => 'HS256');
        }
        $payload = array('iat' => time(), 'user' => $user);
        $jwt = JWT::build($header, $payload, $config['jwt_secret']);
        return $jwt->token;
    }

    /** check from the ident core_module if login is correct.
     * This should be used only from login page/api and use tokens once logged.
     * @return User id if the user and password are correct,
     * null otherwise. */
    public static function login($login, $password) {
        if (function_exists('\Pasteque\core_ident_login')) {
            // Use login from the module instead of the default one
            return core_ident_login($login, $password);
        }
        $user = core_ident_getUser($login);
        if ($user === null || !isset($user['pwd_hash'])) {
            return null;
        }
        if (password_verify($password, $user['pwd_hash'])) {
            Login::$cachedUser = $user['id'];
            return $user['id'];
        }
        return null;
    }

}

/** @deprecated
 * Shortcut to Login::getLoggedUser for backward compatibility. */
function get_user_id() {
    Log::info('Using deprecated function get_user_id');
    return Login::getLoggedUser();
}
