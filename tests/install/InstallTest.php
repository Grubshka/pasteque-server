<?php
//    Pasteque server testing
//
//    Copyright (C) 2017 Philippe Pary (philippe@pasteque.org)
//
//    This file is part of Pasteque.
//
//    Pasteque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pasteque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pasteque.  If not, see <http://www.gnu.org/licenses/>.
namespace Pasteque;

require_once(dirname(dirname(__DIR__)) . "/inc/constants.php");

$config['template'] = 'pasteque-bootstrap';
PT::$ABSPATH = dirname(dirname(__DIR__));
$altConfigFile = "tests/config.php";

require_once(PT::$ABSPATH . "/inc/load.php");
require_once(PT::$ABSPATH . "/inc/Log.php");
require_once(PT::$ABSPATH . "/inc/date_utils.php");
require_once(PT::$ABSPATH . "/inc/url_broker.php");
require_once(PT::$ABSPATH . "/inc/i18n.php");
require_once(PT::$ABSPATH . "/inc/i18n_aliases.php");
require_once(PT::$ABSPATH . "/inc/Report.php");
require_once(PT::$ABSPATH . "/inc/hooks.php");
require_once(PT::$ABSPATH . "/inc/PDOBuilder.php");
require_once(PT::$ABSPATH . "/inc/DB.php");
require_once(PT::$ABSPATH . "/inc/images.php");
require_once(PT::$ABSPATH . "/inc/Module.php");
require_once(PT::$ABSPATH . "/inc/jwt.php");
require_once(PT::$ABSPATH . "/inc/login.php");
require_once(PT::$ABSPATH . '/inc/installer.php');

$arguments["dbtype"] = $config["db_type"];
$arguments["dbuser"] = $config["db_user"];
$arguments["dbpassword"] = $config["db_password"];
$arguments["dbhost"] = $config["db_host"];
$arguments["dbname"] = $config["db_name"];
$arguments["username"] = "admin";
$arguments["userpassword"] = "t0ps3cr3tp4ssw0rD!";
$arguments["debug"] = "999";

Class InstallTest extends \PHPUnit_Framework_TestCase {

    public function testSanitite_n_default() {
        global $arguments;
        $arguments = Installer::sanitize_n_default($arguments);
        $this->assertEquals("mysql",$arguments["dbtype"],"Sanitize and default failed");
    }

    public function testPerform_checks() {
        global $arguments;
        $this->assertNotEquals(1, Installer::perform_checks(),"Perform checks failed");
    }

    public function testDatabase_setup() {
        global $arguments;
        $this->assertNotEquals(1, Installer::database_setup(),"Database setup failed with good credentials");
        $arguments["dbpassword"] = "thisisnotthepasswordIhope";
        $this->assertNotEquals(1, Installer::database_setup(),"Database setup failed with wrong credentials");
        dropDatabase();
        $this->assertNotEquals(1, Installer::database_setup(),"Database creation failed");
    }

    public function testUserconfig_write() {
        global $arguments;
        $this->assertNotEquals(1, Installer::userconfig_write(),"Userconfiguration failed to be written");
    }

    public function testConfigfile_write() {
        global $arguments;
        $this->assertNotEquals(1, Installer::configfile_write(),"Configuration file writing failed");
    }
}
