<?php
//    Pastèque Web back office
//
//    Copyright (C) 2013 Scil (http://scil.coop)
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

// API is the entry point for all API calls.
namespace Pasteque;

require_once(__DIR__ . '/inc/constants.php');
PT::$ABSPATH = __DIR__; // Base path. Also to check if a call
                         // originates from api.php
// Load
require_once(PT::$ABSPATH . '/inc/load.php');
require_once(PT::$ABSPATH . '/inc/load_api.php');

if (isset($_GET[PT::URL_ACTION_PARAM])) {
    $api = $_GET[PT::URL_ACTION_PARAM];
    $params = $_GET;
    if (isset($_GET['action'])) {
        $action = $_GET['action'];
    } else {
        $action = null;
    }
} else {
    if (isset($_POST[PT::URL_ACTION_PARAM])) {
        $api = $_POST[PT::URL_ACTION_PARAM];
        $params = $_POST;
        if (isset($_POST['action'])) {
            $action = $_POST['action'];
        } else {
            $action = null;
        }
    } else {
        $api = null;
    }
}

$broker = new APIBroker($api);
$result = $broker->run($action, $params);

if (array_key_exists('allowed_origin', getConfig()) && array_key_exists('HTTP_ORIGIN', $_SERVER)) {
    // Handle CORS
    $config = getConfig();
    $cors = $config['allowed_origin'];
    $origin = $_SERVER['HTTP_ORIGIN'];
    // Accept *, single value match or in array
    $allowedOrigin = ($cors === '*'
            || (!is_array($cors) && $cors === $origin)
            || (is_array($cors) && in_array($origin, $cors)));
    if ($allowedOrigin) {
        header('Access-Control-Allow-Origin: ' . $origin);
        header('Access-Control-Allow-Credentials: false'); // Turned off for now, api should embed token in requests.
        header('Access-Control-Max-Age: ' . $config['jwt_timeout']);
        if (is_array($cors)) { header('Vary: Origin'); }
    } else {
        Log::info('Call from ' . $origin . ' probably rejected (CORS)');
    }
}
if ($api == 'ImagesAPI') {
    // Special case of images api with binary data
    header('Cache-Control: max-age=864000');
    echo($result);
} else {
    header('Content-type: application/json');
    echo json_encode($result);
}
