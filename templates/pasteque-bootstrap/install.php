<?php
//    Pastèque Web back office
//
//    Copyright (C) 2016 Scil (http://scil.coop)
//          Philippe Pary philippe@scil.coop
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.
namespace Pasteque;

?>
<form class="form-horizontal" action="install.php" method="post">
<fieldset>

<div class="page-header text-center">
  <h1>Installation of Pasteque-server</h1>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="dbuser">Database</label>
  <div class="col-md-4">
  <input id="dbuser" name="dbuser" placeholder="Database user" class="form-control input-md" required="" type="text">
  <span class="help-block">Login for your database</span>  
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="dbpassword"></label>
  <div class="col-md-4">
    <input id="dbpassword" name="dbpassword" placeholder="Database password" class="form-control input-md" required="" type="password">
    <span class="help-block">Password for your database</span>
  </div>
</div>

<div class="form-group">
  <div class="col-md-4 col-md-offset-4">
    <button type="button" id="username_button" name="username_button" class="btn btn-info" onclick="$('#username').slideToggle();false;;"><span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span> User configuration</button>
    <button type="button" id="advanced_options_button" name="advanced_options_button"class="btn btn-default" onclick="$('#advanced_options').slideToggle();false;"><span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span> Advanced options</button>
  </div>
</div>

<div id="username" style="display:none;">
  <div class="form-group">
    <label class="col-md-4 control-label" for="username">Username</label>
    <div class="col-md-4">
    <input id="username" name="username" placeholder="Username" class="form-control input-md" type="text">
    <span class="help-block">username used for logging in Pastèque</span>  
    </div>
  </div>

  <div class="form-group">
    <label class="col-md-4 control-label" for="userpassword"></label>
    <div class="col-md-4">
      <input id="userpassword" name="userpassword" placeholder="Password" class="form-control input-md" type="password">
      <span class="help-block">password used for logging in Pastèque</span>
    </div>
  </div>
</div>


<div id="advanced_options" style="display:none;">
  <div class="form-group">
    <label class="col-md-4 control-label" for="debug">Debug Level</label>
    <div class="col-md-4">
      <select id="debug" name="debug" class="form-control">
        <option value="NONE">None</option>
        <option value="DEBUG">DEBUG</option>
        <option value="INFO">INFO</option>
        <option value="WARNING">WARNING</option>
        <option value="ERROR">ERROR</option>
        <option value="WTF">WTF</option>
      </select>
    </div>
  </div>

  <div class="form-group">
    <label class="col-md-4 control-label" for="dbname"></label>
    <div class="col-md-4">
      <input id="dbname" name="dbname" placeholder="Database name" class="form-control input-md" type="text">
      <span class="help-block">Name of your database. Default it will try either to create a new database and a new database user, either to guess the database name by itself</span>
    </div>
  </div>
</div>

<hr>

<div class="form-group">
  <div class="col-md-4 col-md-offset-4">
    <input type="submit" id="next_step_button" name="next_step_button "class="btn btn-primary" value="Launch Pastèque installation !">
  </div>
</div>

</fieldset>
</form>

